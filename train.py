# Implementation of Wang et al 2017: Automatic Brain Tumor Segmentation using Cascaded Anisotropic Convolutional Neural Networks. https://arxiv.org/abs/1709.00382

# Author: Guotai Wang
# Copyright (c) 2017-2018 University College London, United Kingdom. All rights reserved.
# http://cmictig.cs.ucl.ac.uk
#
# Distributed under the BSD-3 licence. Please see the file licence.txt
# This software is not certified for clinical use.
#
from __future__ import absolute_import, print_function

import os
import sys
import random
import signal
import numpy as np
import tensorflow as tf

from tensorflow.contrib.layers.python.layers import regularizers
from niftynet.layer.loss_segmentation import LossFunction
from util.data_loader import *
from util.train_test_func import *
from util.parse_config import parse_config
from util.MSNet import MSNet
from tf_msnet.tf_msnet import TF_MSNet
from tf_msnet.util.score import compute_score
from tf_msnet.util.globals import get_channel_axis

class NetFactory(object):
    @staticmethod
    def create(name):
        net = {
            'MSNet': MSNet,
            'TF_MSNet': TF_MSNet
        }.get(name, None)

        if net is not None:
            return net

        print('unsupported network:', name)
        exit()

def train(config_file):
    # 1, load configuration parameters
    config = parse_config(config_file)
    config_data  = config['data']
    config_net   = config['network']
    config_train = config['training']
    config_ext   = config.get('ext', None)

    random.seed(config_train.get('random_seed', 1))
    assert(config_data['with_ground_truth'])

    net_type    = config_net['net_type']
    net_name    = config_net['net_name']
    class_num   = config_net['class_num']
    model_save_prefix = config_train['model_save_prefix']
    batch_size  = config_data.get('batch_size', 5)
    full_data_shape  = [batch_size] + config_data['data_shape']
    full_label_shape = [batch_size] + config_data['label_shape']

    type        = config_ext.get('type', 0) if config_ext else 0
    renorm      = config_ext.get('renorm', True) if config_ext else True
    data_format = config_ext.get('data_format', 'channels_last') if config_ext else 'channels_last'
    graph_save_path = config_ext.get('graph_save_path', 'graphs/' + net_name + '/') if config_ext else 'graphs/' + net_name + '/'
    model_save_path = config_ext.get('model_save_path', 'models/' + net_name + '/') if config_ext else 'models/' + net_name + '/'
    axis        = get_channel_axis(data_format)

    # 2, construct graph
    x = tf.placeholder(tf.float32, full_data_shape, 'X')
    w = tf.placeholder(tf.float32, full_label_shape, 'W')
    y = tf.placeholder(tf.int64,   full_label_shape, 'Y')

    w_regularizer = regularizers.l2_regularizer(config_train.get('decay', 1e-7))

    if net_type == 'MSNet':
        net = MSNet(num_classes = class_num,
                    w_regularizer = w_regularizer,
                    name = net_name)
        net.set_params(config_net)
        predicty = net(x, is_training = True)
    elif net_type == 'TF_MSNet':
        predicty = TF_MSNet(
            num_classes = class_num,
            kern_regularizer = w_regularizer,
            name = net_name,
            renorm = renorm,
            data_format = data_format)(x, type, True)

    loss = LossFunction(n_class=class_num)(predicty, y, weight_map=w)
    opt = tf.train.AdamOptimizer(config_train.get('learning_rate', 1e-3))
    opt_step = opt.minimize(loss, tf.train.get_global_step())

    scalars = ['Loss','Dice','Sensitivity','Specificity','Accuracy']
    with tf.variable_scope('Scalars'):
        placeholders = dict([(s, tf.placeholder(tf.float32, [], s)) for s in scalars])
        for k, v in placeholders.items(): tf.summary.scalar(k, v)
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter(graph_save_path, tf.get_default_graph())

    # 3, initialize session and saver
    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()

    dataloader = DataLoader(config_data)
    dataloader.load_data()

    # 4, start to train
    saver = tf.train.Saver()
    start_it = config_train.get('start_iteration', 0)
    if start_it > 0:
        saver.restore(sess, config_train['model_pre_trained'])

    os.makedirs(model_save_path, exist_ok=True)

    def handler(signum, frame):
        print("Got interrupt at {}".format(n+1))
        saver.save(sess, model_save_path + model_save_prefix + ".ckpt", global_step=n+1)
        writer.close()
        sess.close()
        sys.exit()

    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)

    for n in range(start_it, config_train['maximal_iteration']):
        train_pair = dataloader.get_subimage_batch()
        tempx = train_pair['images']
        tempw = train_pair['weights']
        tempy = train_pair['labels']
        opt_step.run(feed_dict={x:tempx, w:tempw, y:tempy})

        if not n % config_train['test_iteration']:
            scores = dict([(s, []) for s in scalars])
            for step in range(config_train['test_step']):
                train_pair = dataloader.get_subimage_batch()
                tempx = train_pair['images']
                tempw = train_pair['weights']
                tempy = train_pair['labels']
                [tempp,templ] = sess.run([predicty,loss], feed_dict={x:tempx, w:tempw, y:tempy})
                score = compute_score(tempp, tempy, tempw.astype(np.int), axis)
                score['Loss'] = templ
                for k, v in scores.items(): v.append(score[k])

            feed_dict = dict([(placeholders[s], np.asarray(scores[s], np.float32).mean()) for s in scalars])
            summary = merged.eval(feed_dict=feed_dict)
            writer.add_summary(summary, n)

        if not (n+1) % config_train['snapshot_iteration']:
            saver.save(sess, model_save_path + model_save_prefix + ".ckpt", global_step=n+1)

    if n+1 % config_train['snapshot_iteration']:
        saver.save(sess, model_save_path + model_save_prefix + ".ckpt", global_step=n+1)
    writer.close()
    sess.close()

if __name__ == '__main__':
    if(len(sys.argv) != 2):
        print('Number of arguments should be 2. e.g.')
        print('    python train.py config/train_wt_ax.txt')
        exit()
    config_file = str(sys.argv[1])
    assert(os.path.isfile(config_file))
    train(config_file)
