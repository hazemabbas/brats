from __future__ import absolute_import
import tensorflow as tf
import numpy as np
import tf_msnet.util.globals as g

from tensorflow.python.layers.layers import BatchNormalization, Conv3D, Conv3DTranspose

class ResBlock(object):
    """
        Parameters:
    """
    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1, 1),
                 dilation_rate=(1, 1, 1),
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 momentum=0.99,
                 epsilon=0.001,
                 renorm=False,
                 renorm_clipping=None,
                 activation=tf.nn.relu,
                 res_fn=None,
                 data_format=g._DATA_FORMAT,
                 name=None):

        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer
        self.momentum = momentum
        self.epsilon = epsilon
        self.renorm = renorm
        self.renorm_clipping = renorm_clipping
        self.activation = activation
        self.res_fn = res_fn
        self.data_format = data_format
        self.name = name

    """
        Parameters:
    """
    def __call__(self, inputs, training=None):

        output = inputs
        with tf.variable_scope(self.name, "ResBlock"):
            if self.res_fn is not None:
                inputs = self.res_fn(inputs)

            for i in range(2):
                brn = BatchNormalization(
                    axis=g.get_channel_axis(self.data_format),
                    momentum=self.momentum,
                    epsilon=self.epsilon,
                    renorm=self.renorm,
                    renorm_clipping=self.renorm_clipping
                )

                conv = Conv3D(
                    self.filters,
                    self.kernel_size,
                    strides = self.strides,
                    padding = 'same',
                    data_format = self.data_format,
                    dilation_rate = self.dilation_rate,
                    use_bias = False,
                    kernel_initializer = self.kernel_initializer,
                    kernel_regularizer = self.kernel_regularizer
                )

                output = brn(output, training=training)
                output = self.activation(output)
                output = conv(output)

            return output + inputs

class ConvBlock(object):
    """
        Parameters:
    """
    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1, 1),
                 dilation_rate=(1, 1, 1),
                 padding='valid',
                 with_bias=False,
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 bias_initializer=None,
                 bias_regularizer=None,
                 with_bn=True,
                 momentum=0.99,
                 epsilon=0.001,
                 renorm=False,
                 renorm_clipping=None,
                 activation=tf.nn.relu,
                 data_format=g._DATA_FORMAT,
                 name=None):

        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.with_bias = with_bias
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer
        self.bias_initializer = bias_initializer
        self.bias_regularizer = bias_regularizer
        self.with_bn = with_bn
        if with_bn:
            self.momentum = momentum
            self.epsilon = epsilon
            self.renorm = renorm
            self.renorm_clipping = renorm_clipping
        self.activation = activation
        self.data_format = data_format
        self.name = name

    """
        Parameters:
    """
    def __call__(self, inputs, training=None):

        with tf.variable_scope(self.name, "ConvBlock"):
            conv = Conv3D(
                self.filters,
                self.kernel_size,
                strides = self.strides,
                padding = self.padding,
                data_format = self.data_format,
                dilation_rate = self.dilation_rate,
                use_bias = self.with_bias,
                kernel_initializer = self.kernel_initializer,
                kernel_regularizer = self.kernel_regularizer,
                bias_initializer = self.bias_initializer,
                bias_regularizer = self.bias_regularizer
            )

            if self.with_bn:
                brn = BatchNormalization(
                    axis=g.get_channel_axis(self.data_format),
                    momentum=self.momentum,
                    epsilon=self.epsilon,
                    renorm=self.renorm,
                    renorm_clipping=self.renorm_clipping
                )

            inputs = conv(inputs)
            if self.with_bn:
                inputs = brn(inputs, training=training)
            if self.activation:
                inputs = self.activation(inputs)

            return inputs

class DeconvBlock(object):
    """
        Parameters:
    """
    def __init__(self,
                 filters,
                 kernel_size,
                 strides=(1, 1, 1),
                 padding='valid',
                 with_bias=False,
                 kernel_initializer=None,
                 kernel_regularizer=None,
                 bias_initializer=None,
                 bias_regularizer=None,
                 with_bn=True,
                 momentum=0.99,
                 epsilon=0.001,
                 renorm=False,
                 renorm_clipping=None,
                 activation=tf.nn.relu,
                 data_format=g._DATA_FORMAT,
                 name=None):

        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.padding = padding
        self.with_bias = with_bias
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer
        self.bias_initializer = bias_initializer
        self.bias_regularizer = bias_regularizer
        self.with_bn = with_bn
        if with_bn:
            self.momentum = momentum
            self.epsilon = epsilon
            self.renorm = renorm
            self.renorm_clipping = renorm_clipping
        self.activation = activation
        self.data_format = data_format
        self.name = name

    """
        Parameters:
    """
    def __call__(self, inputs, training=None):

        with tf.variable_scope(self.name, "DeconvBlock"):
            deconv = Conv3DTranspose(
                self.filters,
                self.kernel_size,
                strides = self.strides,
                padding = self.padding,
                data_format = self.data_format,
                use_bias = self.with_bias,
                kernel_initializer = self.kernel_initializer,
                kernel_regularizer = self.kernel_regularizer,
                bias_initializer = self.bias_initializer,
                bias_regularizer = self.bias_regularizer
            )

            if self.with_bn:
                brn = BatchNormalization(
                    axis=g.get_channel_axis(self.data_format),
                    momentum=self.momentum,
                    epsilon=self.epsilon,
                    renorm=self.renorm,
                    renorm_clipping=self.renorm_clipping
                )

            inputs = deconv(inputs)
            if self.with_bn:
                inputs = brn(inputs, training=training)
            if self.activation:
                inputs = self.activation(inputs)

            return inputs


def ZeroPaddingAxis(shape, axis):
    def _pad(inputs):
        paddings = np.zeros((inputs.shape.ndims, 2))
        s = (shape,) if isinstance(shape, int) else shape
        a = (axis,)  if isinstance(axis,  int) else axis
        for s, a in zip(s, a):
            d = s - inputs.shape[a].value
            paddings[a,:] = d // 2, d - d // 2
        return tf.pad(inputs, tf.constant(paddings, tf.int32))
    return _pad
