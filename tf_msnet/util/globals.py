import tensorflow as tf

_FLOATX = tf.float64
_EPSILON = 1e-7
_DATA_FORMAT = 'channels_first'

get_channel_axis = lambda data_format=None: {
    'NCW': 1, 'NWC': -1,
    'NCHW': 1, 'NHWC': -1,
    'NCDHW': 1, 'NDHWC': -1,
    'channels_first': 1, 'channels_last': -1
}[data_format or _DATA_FORMAT]

get_data_format = lambda channel_axis=None: {
    None: _DATA_FORMAT,
    1: 'channels_first',
    -1: 'channels_last'
}[channel_axis]
